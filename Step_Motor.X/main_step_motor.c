/*
 * File:   main_step_motor.c
 * Author: Aleksejshch
 *
 * Created on 25 ����� 2016 �., 16:31
 */

#include <stdio.h>
#include <stdlib.h>
#include <PIC24F_plib.h>
#include "xc.h"
#include "main_header.h"

//====================CONFIG=======================================================
_CONFIG2(POSCMOD_XT & FCKSM_CSECME & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV2 )
//XT Oscillator mode is selecte
//Clock switching is enabled, Fail-Safe Clock Monitor is enabled
//Primary Oscillator with PLL module (XTPLL, HSPLL, ECPLL)
//96 MHz PLL is enabled automatically on start-up
//Oscillator input is divided by 2 (8 MHz input)
_CONFIG1(FWDTEN_OFF & ICS_PGx3 & JTAGEN_OFF)
//Watchdog Timer is disabled
//Emulator functions are shared with PGEC3/PGED3
//JTAG port is disabled
//==================================================================================




//#define SECOND_LENS     // �������� � ������������ ���������        

int boost_counter;        
int step_counter_motor_1;
int step_counter_motor_2;
int step_counter_motor_3;
int step_counter_motor_4;

void Timer_1_Init (void)
{
    T1CONbits.TON = 0;      // ������ ����
    T1CONbits.TSIDL = 0;    // ������ �������� � ������ ��������� ����
    T1CONbits.TGATE = 0;    // ���������� ������� ������� ��������
    T1CONbits.TCKPS = 0b01;     //11 = 1:256, 10 = 1:64, 01 = 1:8, 00 = 1:1
    T1CONbits.TSYNC = 0;    // �� ����������� ������������� �������� �������
    T1CONbits.TCS = 0;      // �������� �������� ������� - ���������� ��������� Fosc/2
    
//    PR1 = 0x06FF;       // ������� �������
    PR1 = 2500;       // ������� ������� (1.25 ms))
//    PR1 = 625;       // ������� ������� (312.5 mks))
    
    _T1IF = 0;      //T1_Clear_Intr_Status_Bit;
    _T1IP =5;       //SetPriorityIntT1(5);
    _T1IE = 1;      //EnableIntT1;
    T1CONbits.TON = 1;      // ������ ���
}

void Timer_2_Init (void)
{
    T2CONbits.TON = 0;      // ������ ����
    T2CONbits.TSIDL = 0;    // ������ �������� � ������ ��������� ����
    T2CONbits.TGATE = 0;    // ���������� ������� ������� ��������
    T2CONbits.TCKPS = 0b11;     //11 = 1:256, 10 = 1:64, 01 = 1:8, 00 = 1:1
    //T2CONbits.TSYNC = 0;    // �� ����������� ������������� �������� �������
    T2CONbits.TCS = 0;      // �������� �������� ������� - ���������� ��������� Fosc/2
    
    PR2 = 0x0FFF;       // ������� �������
    
    _T2IF = 0;      //T1_Clear_Intr_Status_Bit;
    _T2IP =5;       //SetPriorityIntT1(5);
    _T2IE = 1;      //EnableIntT1;
    T2CONbits.TON = 1;      // ������ ���
}
 

int main(void) 
{
    CLKDIVbits.PLLEN = 1;//Enable PPL
    CLKDIVbits.CPDIV = 0b00; //32MHz(16MIPS)
    CLKDIVbits.DOZE = 0b000;
    ANSB = 0x0000;
    ANSC = 0x0000;
    ANSD = 0x0000;
    ANSF = 0x0000;
    ANSG = 0x0000;
    LED_1_CONFIG;
    LED_1_OFF;
    LED_2_CONFIG;
    LED_2_OFF;
    LED_3_CONFIG;
    LED_3_OFF;
    
    MOTORS_INITPIN();
    BUTTONS_INITPIN();
    Timer_1_Init();
    #ifdef SECOND_LENS
        Timer_2_Init();
    #endif
    step_counter_motor_1 = 1;
    step_counter_motor_2 = 1;
    step_counter_motor_3 = 1;
    step_counter_motor_4 = 1;
    
    while (1)
    {
        
    }
    return 0;
}


#ifdef SECOND_LENS
    void __attribute__ ((interrupt, no_auto_psv)) _T2Interrupt(void)
    {
        _T2IF = 0;      //T1_Clear_Intr_Status_Bit;
        TMR2 = 0;
        
        /*      ��������� ������ 1      */
        //���� ������ "FORVARD_MOTOR_1"
        if ((!Is_BUTTON_FORVARD_MOTOR_1_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_1_UNPRESSED))
        {
            if (step_counter_motor_1 < 4)   step_counter_motor_1++;
            else    step_counter_motor_1 = 1;
            switch (step_counter_motor_1)
            {
                case 1:     MOTOR_1_STOP(); MOTOR_1_STEP_ONE();     break;
                case 2:     MOTOR_1_STOP(); MOTOR_1_STEP_TWO();     break;
                case 3:     MOTOR_1_STOP(); MOTOR_1_STEP_THREE();   break;
                case 4:     MOTOR_1_STOP(); MOTOR_1_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� ������ "BACKVARD_MOTOR_1"
        if ((Is_BUTTON_FORVARD_MOTOR_1_UNPRESSED) && (!Is_BUTTON_BACKVARD_MOTOR_1_UNPRESSED))
        {
            if (step_counter_motor_1 > 1)   step_counter_motor_1--;
            else    step_counter_motor_1 = 4;
            switch (step_counter_motor_1)
            {
                case 1:     MOTOR_1_STOP(); MOTOR_1_STEP_ONE();     break;
                case 2:     MOTOR_1_STOP(); MOTOR_1_STEP_TWO();     break;
                case 3:     MOTOR_1_STOP(); MOTOR_1_STEP_THREE();   break;
                case 4:     MOTOR_1_STOP(); MOTOR_1_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� �� ���� �� ������ ������ 1 �� ������
        if ((Is_BUTTON_FORVARD_MOTOR_1_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_1_UNPRESSED))
        { MOTOR_1_STOP(); }  
    }
#endif


void __attribute__ ((interrupt, no_auto_psv)) _T1Interrupt(void)
{
    LED_1_ON;
    _T1IF = 0;      //T1_Clear_Intr_Status_Bit;
    TMR1 = 0;
    
    #ifdef SECOND_LENS
        if ((!Is_BUTTON_FORVARD_MOTOR_2_UNPRESSED) ||
                (!Is_BUTTON_BACKVARD_MOTOR_2_UNPRESSED) ||
                (!Is_BUTTON_FORVARD_MOTOR_3_UNPRESSED) ||
                (!Is_BUTTON_BACKVARD_MOTOR_3_UNPRESSED))//���� ������ ���� �����-��
        {
            if (boost_counter >= 1)
            {
                boost_counter--;
            }
            PR1 = PERIOD_MIN + (boost_counter * PERIOD_STEP);
        }
        else
        {
            boost_counter = PERIOD_COUNT_NUM;
            PR1 = PERIOD_MIN + (PERIOD_STEP * PERIOD_COUNT_NUM);
        }
    #else 
        /*      ��������� ������ 1      */
        //���� ������ "FORVARD_MOTOR_1"
        if ((!Is_BUTTON_FORVARD_MOTOR_1_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_1_UNPRESSED))
        {
            if (step_counter_motor_1 < 4)   step_counter_motor_1++;
            else    step_counter_motor_1 = 1;
            switch (step_counter_motor_1)
            {
                case 1:     MOTOR_1_STOP(); MOTOR_1_STEP_ONE();     break;
                case 2:     MOTOR_1_STOP(); MOTOR_1_STEP_TWO();     break;
                case 3:     MOTOR_1_STOP(); MOTOR_1_STEP_THREE();   break;
                case 4:     MOTOR_1_STOP(); MOTOR_1_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� ������ "BACKVARD_MOTOR_1"
        if ((Is_BUTTON_FORVARD_MOTOR_1_UNPRESSED) && (!Is_BUTTON_BACKVARD_MOTOR_1_UNPRESSED))
        {
            if (step_counter_motor_1 > 1)   step_counter_motor_1--;
            else    step_counter_motor_1 = 4;
            switch (step_counter_motor_1)
            {
                case 1:     MOTOR_1_STOP(); MOTOR_1_STEP_ONE();     break;
                case 2:     MOTOR_1_STOP(); MOTOR_1_STEP_TWO();     break;
                case 3:     MOTOR_1_STOP(); MOTOR_1_STEP_THREE();   break;
                case 4:     MOTOR_1_STOP(); MOTOR_1_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� �� ���� �� ������ ������ 1 �� ������
        if ((Is_BUTTON_FORVARD_MOTOR_1_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_1_UNPRESSED))
        { MOTOR_1_STOP(); }  
    #endif
        /*      ��������� ������ 2      */
        //���� ������ "FORVARD_MOTOR_2"
        if ((!Is_BUTTON_FORVARD_MOTOR_2_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_2_UNPRESSED))
        {
            if (step_counter_motor_2 < 4)   step_counter_motor_2++;
            else    step_counter_motor_2 = 1;
            switch (step_counter_motor_2)
            {
                case 1:     MOTOR_2_STOP(); MOTOR_2_STEP_ONE();     break;
                case 2:     MOTOR_2_STOP(); MOTOR_2_STEP_TWO();     break;
                case 3:     MOTOR_2_STOP(); MOTOR_2_STEP_THREE();   break;
                case 4:     MOTOR_2_STOP(); MOTOR_2_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� ������ "BACKVARD_MOTOR_2"
        if ((Is_BUTTON_FORVARD_MOTOR_2_UNPRESSED) && (!Is_BUTTON_BACKVARD_MOTOR_2_UNPRESSED))
        {
            if (step_counter_motor_2 > 1)   step_counter_motor_2--;
            else    step_counter_motor_2 = 4;
            switch (step_counter_motor_2)
            {
                case 1:     MOTOR_2_STOP(); MOTOR_2_STEP_ONE();     break;
                case 2:     MOTOR_2_STOP(); MOTOR_2_STEP_TWO();     break;
                case 3:     MOTOR_2_STOP(); MOTOR_2_STEP_THREE();   break;
                case 4:     MOTOR_2_STOP(); MOTOR_2_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� �� ���� �� ������ ������ 2 �� ������
        if ((Is_BUTTON_FORVARD_MOTOR_2_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_2_UNPRESSED))
        { MOTOR_2_STOP(); }


        /*      ��������� ������ 3      */
        //���� ������ "FORVARD_MOTOR_3"
        if ((!Is_BUTTON_FORVARD_MOTOR_3_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_3_UNPRESSED))
        {
            if (step_counter_motor_3 < 4)   step_counter_motor_3++;
            else    step_counter_motor_3 = 1;
            switch (step_counter_motor_3)
            {
                case 1:     MOTOR_3_STOP(); MOTOR_3_STEP_ONE();     break;
                case 2:     MOTOR_3_STOP(); MOTOR_3_STEP_TWO();     break;
                case 3:     MOTOR_3_STOP(); MOTOR_3_STEP_THREE();   break;
                case 4:     MOTOR_3_STOP(); MOTOR_3_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� ������ "BACKVARD_MOTOR_3"
        if ((Is_BUTTON_FORVARD_MOTOR_3_UNPRESSED) && (!Is_BUTTON_BACKVARD_MOTOR_3_UNPRESSED))
        {
            if (step_counter_motor_3 > 1)   step_counter_motor_3--;
            else    step_counter_motor_3 = 4;
            switch (step_counter_motor_3)
            {
                case 1:     MOTOR_3_STOP(); MOTOR_3_STEP_ONE();     break;
                case 2:     MOTOR_3_STOP(); MOTOR_3_STEP_TWO();     break;
                case 3:     MOTOR_3_STOP(); MOTOR_3_STEP_THREE();   break;
                case 4:     MOTOR_3_STOP(); MOTOR_3_STEP_FOUR();    break;
                default:                    break;
            }
        }

        //���� �� ���� �� ������ ������ 3 �� ������
        if ((Is_BUTTON_FORVARD_MOTOR_3_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_3_UNPRESSED))
        { MOTOR_3_STOP(); }
    
    
    /*      ��������� ������ 4      */
    //���� ������ "FORVARD_MOTOR_4"
    if ((!Is_BUTTON_FORVARD_MOTOR_4_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_4_UNPRESSED))
    {
        if (step_counter_motor_4 < 4)   step_counter_motor_4++;
        else    step_counter_motor_4 = 1;
        switch (step_counter_motor_4)
        {
            case 1:     MOTOR_4_STOP(); MOTOR_4_STEP_ONE();     break;
            case 2:     MOTOR_4_STOP(); MOTOR_4_STEP_TWO();     break;
            case 3:     MOTOR_4_STOP(); MOTOR_4_STEP_THREE();   break;
            case 4:     MOTOR_4_STOP(); MOTOR_4_STEP_FOUR();    break;
            default:                    break;
        }
    }
    
    //���� ������ "BACKVARD_MOTOR_4"
    if ((Is_BUTTON_FORVARD_MOTOR_4_UNPRESSED) && (!Is_BUTTON_BACKVARD_MOTOR_4_UNPRESSED))
    {
        if (step_counter_motor_4 > 1)   step_counter_motor_4--;
        else    step_counter_motor_4 = 4;
        switch (step_counter_motor_4)
        {
            case 1:     MOTOR_4_STOP(); MOTOR_4_STEP_ONE();     break;
            case 2:     MOTOR_4_STOP(); MOTOR_4_STEP_TWO();     break;
            case 3:     MOTOR_4_STOP(); MOTOR_4_STEP_THREE();   break;
            case 4:     MOTOR_4_STOP(); MOTOR_4_STEP_FOUR();    break;
            default:                    break;
        }
    }
    
    //���� �� ���� �� ������ ������ 4 �� ������
    if ((Is_BUTTON_FORVARD_MOTOR_4_UNPRESSED) && (Is_BUTTON_BACKVARD_MOTOR_4_UNPRESSED))
    { MOTOR_4_STOP(); }
    LED_1_OFF;
}